﻿using System;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using IcsManagerLibrary;

namespace IcsManagerConsole
{

    class Program
    {
        public static void logIt(String msg)
        {
            System.Diagnostics.Trace.WriteLine(msg);
        }

        static void Info()
        {
            foreach (var nic in IcsManager.GetIPv4EthernetAndWirelessInterfaces())
            {
                Console.WriteLine(
                            "Name .......... : {0}", nic.Name);
                Console.WriteLine(
                            "GUID .......... : {0}", nic.Id);
                Console.WriteLine(
                            "Status ........ : {0}", nic.OperationalStatus);

                Console.WriteLine(
                            "InterfaceType . : {0}", nic.NetworkInterfaceType);

                if (nic.OperationalStatus == OperationalStatus.Up)
                {
                    var ipprops = nic.GetIPProperties();
                    foreach (var a in ipprops.UnicastAddresses)
                    {
                        if (a.Address.AddressFamily == AddressFamily.InterNetwork)
                            Console.WriteLine(
                                "Unicast address : {0}/{1}", a.Address, a.IPv4Mask);
                    }
                    foreach (var a in ipprops.GatewayAddresses)
                    {
                        Console.WriteLine(
                            "Gateway ....... : {0}", a.Address);
                    }
                }
                try
                {
                    var connection = IcsManager.GetConnectionById(nic.Id);
                    if (connection != null)
                    {
                        var props = IcsManager.GetProperties(connection);
                        Console.WriteLine(
                            "Device ........ : {0}", props.DeviceName);
                        var sc = IcsManager.GetConfiguration(connection);
                        if (sc.SharingEnabled)
                            Console.WriteLine(
                                "SharingType ... : {0}", sc.SharingConnectionType);
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    Console.WriteLine("Please run this program with Admin rights to see all properties");
                }
                catch (NotImplementedException e)
                {
                    Console.WriteLine("This feature is not supported on your operating system.");
                    Console.WriteLine(e.StackTrace);
                }
                Console.WriteLine();
            }
        }



        static int EnableICS(string shared, string home, bool force)
        {
            var connectionToShare = IcsManager.FindConnectionByIdOrName(shared);
            if (connectionToShare == null)
            {
                IcsManager.logIt($"Connection not found: {shared}");
                return 100;
            }
            var homeConnection = IcsManager.FindConnectionByIdOrName(home);
            if (homeConnection == null)
            {
                IcsManager.logIt($"Connection not found: {home}");
                return 101;
            }

            var currentShare = IcsManager.GetCurrentlySharedConnections();
            if (currentShare.Exists)
            {
                IcsManager.logIt("Internet Connection Sharing is already enabled:");
                IcsManager.logIt(currentShare.ToString());
                if (!force)
                {
                    IcsManager.logIt("Please disable it if you want to configure sharing for other connections");
                    return 102;
                }
                IcsManager.logIt("Sharing will be disabled first.");
            }

            IcsManager.ShareConnection(connectionToShare, homeConnection);
            return 0;
        }

        static void DisableICS()
        {
            var currentShare = IcsManager.GetCurrentlySharedConnections();
            if (!currentShare.Exists)
            {
                Console.WriteLine("Internet Connection Sharing is already disabled");
                return;
            }
            Console.WriteLine("Internet Connection Sharing will be disabled:");
            Console.WriteLine(currentShare);
            IcsManager.ShareConnection(null, null);
        }

        static void Usage()
        {
            var appName = Path.GetFileNameWithoutExtension(AppDomain.CurrentDomain.FriendlyName);
            appName = appName == null ? "" : appName.ToLower();
            Console.WriteLine("Usage: ");
            Console.WriteLine("  {0} -info", appName);
            Console.WriteLine("  {0} -enable -shared={{GUID-OF-CONNECTION-TO-SHARE}} -home={{GUID-OF-HOME-CONNECTION}} [-force]", appName);
            Console.WriteLine("  {0} -enable -shared=\"Name of connection to share\" -home=\"Name of home connection\" [-force]", appName);
            Console.WriteLine("  {0} -disable", appName);
        }

        static int Main(string[] args)
        {
            System.Configuration.Install.InstallContext _args = new System.Configuration.Install.InstallContext(null, args);

            int ret = 1;
            if (_args.IsParameterTrue("debug"))
            {
                System.Console.WriteLine("Wait for debug, press any key to continue...");
                System.Console.ReadKey();
            }


            try
            {
                if (args.Length == 0)
                {
                    Info();
                    Usage();
                    return ret;
                }

                if (_args.IsParameterTrue("info"))
                {
                    Info();
                }
                else if (_args.IsParameterTrue("enable"))
                {
                    var force = false;
                    if (_args.IsParameterTrue("force"))
                    {
                        force = true;
                    }
                    try
                    {
                        string shared, home;
                        if (_args.Parameters.ContainsKey("shared") && _args.Parameters.ContainsKey("home"))
                        {
                            shared = _args.Parameters["shared"];
                            home = _args.Parameters["home"];
                        }
                        else
                        {
                            Tuple<bool, string, string> sharehome  = IcsManager.network_configuration();
                            if (sharehome.Item1)
                            {
                                shared = sharehome.Item2;
                                home = sharehome.Item3;
                            }
                            else
                            {
                                return 123;
                            }
                        }
                        logIt($"shared={shared} home={home}\n");
                        return EnableICS(shared, home, force);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        Usage();
                        return 11;
                    }
                    catch (UnauthorizedAccessException)
                    {
                        Console.WriteLine("This operation requires elevation.");
                        return 12;
                    }

                }
                else if (_args.IsParameterTrue("disable"))
                {
                    try
                    {
                        DisableICS();
                        return 0;
                    }
                    catch (UnauthorizedAccessException)
                    {
                        Console.WriteLine("This operation requires elevation.");
                        return 12;
                    }
                }
            }
            catch (NotImplementedException)
            {
                Console.WriteLine("This program is not supported on your operating system.");
                ret = 13;
            }
            finally
            {
#if (DEBUG)
                Console.ReadKey();
#endif
                ret = 15;
            }
                return ret;
        }
    }
}
