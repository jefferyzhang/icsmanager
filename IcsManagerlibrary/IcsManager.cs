﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using NETCONLib;

namespace IcsManagerLibrary
{
    public class IcsManager
    {
        public static void logIt(String msg)
        {
            System.Diagnostics.Trace.WriteLine(msg);
        }

        private static readonly INetSharingManager SharingManager = new NetSharingManager();

        public static IEnumerable<NetworkInterface> GetIPv4EthernetAndWirelessInterfaces()
        {
            return
                from nic in NetworkInterface.GetAllNetworkInterfaces()
                where nic.Supports(NetworkInterfaceComponent.IPv4)
                where (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                   || (nic.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                select nic;
        }

        public static NetShare GetCurrentlySharedConnections()
        {
            INetConnection sharedConnection = (
                from INetConnection c in SharingManager.EnumEveryConnection
                where GetConfiguration(c).SharingEnabled
                where GetConfiguration(c).SharingConnectionType ==
                    tagSHARINGCONNECTIONTYPE.ICSSHARINGTYPE_PUBLIC
                select c).DefaultIfEmpty(null).First();
            INetConnection homeConnection = (
                from INetConnection c in SharingManager.EnumEveryConnection
                where GetConfiguration(c).SharingEnabled
                where GetConfiguration(c).SharingConnectionType ==
                    tagSHARINGCONNECTIONTYPE.ICSSHARINGTYPE_PRIVATE
                select c).DefaultIfEmpty(null).First();
            return new NetShare(sharedConnection, homeConnection);
        }

        public static void ShareConnection(INetConnection connectionToShare, INetConnection homeConnection)
        {
            if ((connectionToShare == homeConnection) && (connectionToShare != null))
                throw new ArgumentException("Connections must be different");
            var share = GetCurrentlySharedConnections();
            if (share.SharedConnection != null)
                GetConfiguration(share.SharedConnection).DisableSharing();
            if (share.HomeConnection != null)
                GetConfiguration(share.HomeConnection).DisableSharing();
            if (connectionToShare != null)
            {
                var sc = GetConfiguration(connectionToShare);
                sc.EnableSharing(tagSHARINGCONNECTIONTYPE.ICSSHARINGTYPE_PUBLIC);
            }
            if (homeConnection != null)
            {
                var hc = GetConfiguration(homeConnection);
                hc.EnableSharing(tagSHARINGCONNECTIONTYPE.ICSSHARINGTYPE_PRIVATE);
            }
        }

        public static INetSharingConfiguration GetConfiguration(INetConnection connection)
        {
            return SharingManager.get_INetSharingConfigurationForINetConnection(connection);
        }

        public static INetConnectionProps GetProperties(INetConnection connection)
        {
            return SharingManager.get_NetConnectionProps(connection);
        }


        public static INetSharingEveryConnectionCollection GetAllConnections()
        {
            return SharingManager.EnumEveryConnection;
        }

        public static INetConnection FindConnectionByIdOrName(string shared)
        {
            return GetConnectionById(shared) ?? GetConnectionByName(shared);
        }

        public static INetConnection GetConnectionById(string guid)
        {
            return (from INetConnection c in GetAllConnections()
                    where GetProperties(c).Guid == guid
                    select c).DefaultIfEmpty(null).First();
        }

        public static INetConnection GetConnectionByName(string name)
        {
            return (from INetConnection c in GetAllConnections()
                    where GetProperties(c).Name == name
                    select c).DefaultIfEmpty(null).First();
        }

        public static string[] runExe(string exeFilename, string param, out int exitCode, System.Collections.Specialized.StringDictionary env = null, int timeout = 60 * 1000, bool syscommand = false)
        {
            List<string> ret = new List<string>();
            exitCode = 1;
            logIt(string.Format("[runExe]: ++ exe={0}, param={1}", exeFilename, param));
            try
            {
                if (syscommand || System.IO.File.Exists(exeFilename))
                {
                    System.Threading.AutoResetEvent ev = new System.Threading.AutoResetEvent(false);
                    Process p = new Process();
                    p.StartInfo.FileName = exeFilename;
                    p.StartInfo.Arguments = param;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.CreateNoWindow = true;
                    if (env != null && env.Count > 0)
                    {
                        foreach (DictionaryEntry de in env)
                        {
                            p.StartInfo.EnvironmentVariables.Add(de.Key as string, de.Value as string);
                        }
                    }
                    p.OutputDataReceived += (obj, args) =>
                    {
                        if (!string.IsNullOrEmpty(args.Data))
                        {
                            logIt(string.Format("[runExe]: {0}", args.Data));
                            ret.Add(args.Data);
                        }
                        if (args.Data == null)
                            ev.Set();
                    };
                    p.Start();
                    p.BeginOutputReadLine();
                    if (p.WaitForExit(timeout))
                    {
                        ev.WaitOne(timeout);
                        if (!p.HasExited)
                        {
                            exitCode = 1460;
                            p.Kill();
                        }
                        else
                            exitCode = p.ExitCode;
                    }
                    else
                    {
                        if (!p.HasExited)
                        {
                            p.Kill();
                        }
                        exitCode = 1460;
                        logIt(string.Format("[runExe]: Kill by caller due to timeout={0}", timeout));
                    }
                }
            }
            catch (Exception ex)
            {
                logIt(string.Format("[runExe]: {0}", ex.Message));
                logIt(string.Format("[runExe]: {0}", ex.StackTrace));
            }
            logIt(string.Format("[runExe]: -- ret={0}", exitCode));
            return ret.ToArray();
        }


        public static Tuple<bool, string, string> network_configuration()
        {
            bool ret = false;
            Tuple<bool, ManagementObject, NETCONLib.INetConnectionProps, NETCONLib.INetSharingConfiguration> lan_interface = new Tuple<bool, ManagementObject, NETCONLib.INetConnectionProps, NETCONLib.INetSharingConfiguration>(false, null, null, null);
            Tuple<bool, NetworkInterface, NETCONLib.INetConnectionProps, NETCONLib.INetSharingConfiguration> wan_interface = new Tuple<bool, NetworkInterface, NETCONLib.INetConnectionProps, NETCONLib.INetSharingConfiguration>(false, null, null, null);
            NETCONLib.NetSharingManager ics = new NETCONLib.NetSharingManager();
            string lan_name = "";
            string wan_name = "";
            // find usb lan adaptor
            try
            {
                ManagementClass mc = new ManagementClass("Win32_NetworkAdapter");
                foreach (ManagementObject mo in mc.GetInstances())
                {
                    string pnpid = mo.Properties["PNPDeviceID"]?.Value?.ToString()?.ToUpper();
                    if (!string.IsNullOrEmpty(pnpid) && pnpid.StartsWith("USB"))
                    {
                        foreach (NETCONLib.INetConnection nc in ics.EnumEveryConnection)
                        {
                            NETCONLib.INetConnectionProps p = ics.NetConnectionProps[nc];
                            NETCONLib.INetSharingConfiguration s = ics.INetSharingConfigurationForINetConnection[nc];
                            //dumpNic(p, s);
                            if (string.Compare(p.Guid, mo.Properties["GUID"]?.Value?.ToString(), true) == 0)
                            {
                                lan_interface = new Tuple<bool, ManagementObject, NETCONLib.INetConnectionProps, NETCONLib.INetSharingConfiguration>(true, mo, p, s);
                                break;
                            }
                        }
                    }
                    /*
                   logIt($"Name={mo.Properties["Name"]?.Value?.ToString()}");
                   logIt($"PhysicalAdapter={mo.Properties["PhysicalAdapter"]?.Value?.ToString()}");
                   logIt($"GUID={mo.Properties["GUID"]?.Value?.ToString()}");
                   logIt($"PNPDeviceID={mo.Properties["PNPDeviceID"]?.Value?.ToString()}");
                   logIt($"Caption={mo.Properties["Caption"]?.Value?.ToString()}");
                    */
                }
            }
            catch (Exception ex)
            {
               logIt($"network_config: find usb-to-lan adaptor: {ex.Message}");
               logIt($"network_config: find usb-to-lan adaptor: {ex.StackTrace}");
            }
            // find internet connection
            if (lan_interface.Item1)
            {
                try
                {
                    List<Task<int>> tasks = new List<Task<int>>();
                    NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
                    foreach (NetworkInterface adapter in adapters)
                    {
                        if (adapter.OperationalStatus == OperationalStatus.Up &&
                            adapter.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                        {
                            if (string.Compare(adapter.Name, lan_interface.Item2.Properties["NetConnectionID"]?.Value?.ToString(), true) == 0)
                                continue;
                            IPInterfaceProperties ipip = adapter.GetIPProperties();
                            foreach (var p in ipip.UnicastAddresses)
                            {
                                if (p.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                                {
                                    Tuple<NetworkInterface, IPAddress> pp = new Tuple<NetworkInterface, IPAddress>(adapter, p.Address);
                                    // check ip 
                                    Task<int> a = Task<int>.Factory.StartNew((o) =>
                                    {
                                        Tuple<NetworkInterface, IPAddress> oo = (Tuple<NetworkInterface, IPAddress>)o;
                                        int err = 1;
                                        string tool = System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.System), "ping.exe");
                                        string[] lines = runExe(tool, $"-n 1 -S {oo.Item2.ToString()} cmc.futuredial.com", out err);
                                        return err;
                                    }, pp);
                                    tasks.Add(a);
                                }
                            }
                        }
                    }
                    Task.WaitAll(tasks.ToArray());
                    foreach (Task<int> t in tasks)
                    {
                        if (t.Result == 0)
                        {
                            Tuple<NetworkInterface, IPAddress> o = (Tuple<NetworkInterface, IPAddress>)t.AsyncState;
                            foreach (NETCONLib.INetConnection nc in ics.EnumEveryConnection)
                            {
                                NETCONLib.INetConnectionProps p = ics.NetConnectionProps[nc];
                                NETCONLib.INetSharingConfiguration s = ics.INetSharingConfigurationForINetConnection[nc];
                                //dumpNic(p, s);
                                if (string.Compare(p.Guid, o.Item1.Id, true) == 0)
                                {
                                    wan_interface = new Tuple<bool, NetworkInterface, NETCONLib.INetConnectionProps, NETCONLib.INetSharingConfiguration>(true, o.Item1, p, s);
                                    break;
                                }
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                   logIt($"network_config: find internet connection: {ex.Message}");
                   logIt($"network_config: find internet connection: {ex.StackTrace}");
                }
                if (lan_interface.Item1 && wan_interface.Item1)
                {
                    // found both interface. dump information.
                   logIt("LAN interface:");
                    foreach (var v in lan_interface.Item2.Properties)
                    {
                       logIt($"\t{v.Name}={v.Value?.ToString()}");
                    }
                   logIt($"\tShared={lan_interface.Item4.SharingEnabled}");
                   logIt($"\tSharedType={lan_interface.Item4.SharingConnectionType}");
                   logIt("WAN interface:");
                   logIt($"\tName={wan_interface.Item2.Name}");
                   logIt($"\tDescription={wan_interface.Item2.Description}");
                   logIt($"\tGUID={wan_interface.Item2.Id}");
                   logIt($"\tType={wan_interface.Item2.NetworkInterfaceType}");
                   logIt($"\tStatus={wan_interface.Item2.OperationalStatus}");
                   logIt($"\tShared={wan_interface.Item4.SharingEnabled}");
                   logIt($"\tSharedType={wan_interface.Item4.SharingConnectionType}");
                   ret = true;
                   lan_name = lan_interface.Item2.Properties["NetConnectionID"].Value.ToString();
                   wan_name = wan_interface.Item2.Name;
                }
            }
            else
            {
                logIt("Fail to find USB-to-LAN adaptor.");
            }
            return new Tuple<bool, string, string>(ret, wan_name, lan_name);
        }

    }
}
